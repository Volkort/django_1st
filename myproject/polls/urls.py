from django.urls import path, include
from polls import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'question', views.QuestionViewSet)


urlpatterns =[
    path('', views.index),
    path('api/v1/', include(router.urls))
]