from django.shortcuts import render
from django.http import HttpResponse

from rest_framework import viewsets

from polls.models import Question
from polls.serializers import QuestionSerializer

# Create your views here.
def index(request):
    return HttpResponse("Bienvenue sur l'application Sondages !")

class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all().order_by("question_text")
    serializer_class = QuestionSerializer
    